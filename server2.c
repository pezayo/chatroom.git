#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <pthread.h>

#define SERVER_PORT 5555

int serverSocket; //服务器socket
int fds[100];     //客户端的socketfd,100个元素，fds[0]~fds[99]
int size = 100;   //用来控制进入聊天室的人数为100以内
int onLineUser[100] = {};
int onLineUserNum = 0;
char *IP = "127.0.0.1";
typedef struct sockaddr SA;

typedef struct userdata
{
    char username[10];
    char password[7];
} Userdata;

void init()
{
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket == -1)
    {
        perror("创建socket失败");
        exit(-1);
    }
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(SERVER_PORT);
    addr.sin_addr.s_addr = inet_addr(IP);
    if (bind(serverSocket, (SA *)&addr, sizeof(addr)) == -1)
    {
        perror("绑定失败");
        exit(-1);
    }
    if (listen(serverSocket, 100) == -1)
    {
        perror("设置监听失败");
        exit(-1);
    }
}

//请求注册时用户信息写入
void datarecord(char *username, char *password)
{
    FILE *fp = fopen("userdata.txt", "a+");
    fprintf(fp, "%-16s", username);
    fputs(password, fp);
    fputc('\n', fp);
    fclose(fp);
    fp = NULL;
}

void SendMsgToAll(char *msg)
{
    int i;
    for (i = 0; i < size; i++)
    {
        if (fds[i] != 0)
        {
            printf("sendto%d\n", fds[i]);
            send(fds[i], msg, strlen(msg), 0);
        }
    }
}

void *service_thread(void *p)
{
    int fd = *(int *)p;
    int m = 0;
    printf("pthread = %d\n", fd);
    loginregisterverify(fd);
    while (1)
    {
        char buf[100] = {};
        if (recv(fd, buf, sizeof(buf), 0) <= 0)
        {
            int i;
            for (i = 0; i < size; i++)
            {
                if (fd == fds[i])
                {
                    fds[i] = 0;
                    break;
                }
            }
            printf("退出：fd = %dquit\n", fd);
            pthread_exit((void *)i);
        }
        //把服务器接受到的信息发给所有的客户端
        SendMsgToAll(buf);
    }
}

//客户端登录和注册请求的回答。服务器拥有一个用户目录，根据用户目录的信息反馈是否同意其请求
void loginregisterverify(int client)
{
    //接受序列化的Userdata的结构体指针
    char *userbuffer = (char *)malloc(sizeof(Userdata));
    //用户数据传输最大字节数
    int needRecv = sizeof(Userdata);
    //接收用户数据结构体
    Userdata *temp = (Userdata *)malloc(needRecv);

    // client = accept(serverSocket, (struct sockaddr *)&clientAddr, (socklen_t *)&len);
    if (client < 0)
    {
        perror("accept");
    }
    // printf("\nrecv client data...n");
    // printf("IP is %s\n", inet_ntoa(clientAddr.sin_addr));
    // printf("Port is %d\n", htons(clientAddr.sin_port));
    //记录登录还是注册
    char option;
    recv(client, &option, sizeof(char), 0);
    printf("the option is %s\n", option == '1' ? "login" : "register");
    char username[10] = {};
    char password[7] = {};
    FILE *fp = NULL;
    int flag = 0; //返回状态信息给客户端
    char buff[100] = {};
label:
    recv(client, userbuffer, sizeof(Userdata), 0);
    memcpy(temp, userbuffer, needRecv); //对接收的数据转换为需要的结构体变量
    printf("the username is %s\nthe password is %s\n", temp->username, temp->password);
    fp = fopen("userdata.txt", "r");
    if (option == '1')
    {
        int count = 0;
        while (1)
        {
            if (fscanf(fp, "%s", username) != EOF)
            {
                count++;
                int x = 0;
                // printf("f1 is %s\n", username);
                if (strcmp(username, temp->username) == 0)
                {
                    printf("找到了这个用户名！\n");
                    printf("%d\n", count);
                    for (int i = 0; i < onLineUserNum; i++)
                    {
                        if (count == onLineUser[i])
                        {
                            printf("用户已登录\n");
                            x = 1;
                            break;
                        }
                    }
                    if (x == 1)
                    {
                        flag = 2;
                        break;
                    }
                    //继续读取用户名后的密码信息
                    fscanf(fp, "%s", password);
                    if (strcmp(password, temp->password) == 0)
                    {
                        printf("密码正确，认证成功\n");
                        onLineUser[onLineUserNum++] = count;
                        flag = 1;
                        break;
                    }
                    else
                    {
                        printf("密码错误，认证失败\n");
                        flag = -1;
                        break;
                    }
                }
            }
            else
            {
                printf("没有找到这个用户名\n");
                flag = 0;
                break;
            }
            fgets(buff, 100, fp);
        }
        fclose(fp);
        fp = NULL;
        send(client, &flag, sizeof(flag), 0);
    }
    else
    {
        while (1)
        {
            if (fscanf(fp, "%s", username) != EOF)
            {
                printf("f1 is %s\n", username);
                if (strcmp(username, temp->username) == 0)
                {
                    printf("用户已存在\n");
                    flag = 0;
                    fclose(fp);
                    fp = NULL;
                    break;
                }
            }
            else
            {
                printf("用户不存在，正在创建用户\n");
                fclose(fp);
                fp = NULL;
                flag = 1;
                datarecord(temp->username, temp->password);
                printf("用户创建成功！\n");
                break;
            }
            fgets(buff, 100, fp); //仅仅用于使fp文件指针换行，便于fscanf读取行首的用户名信息
        }
        send(client, &flag, sizeof(flag), 0);
    }
    if (flag != 1)
        goto label; //请求非法，重新等待客户端请求
}

void service()
{
    printf("服务器启动\n");
    while (1)
    {
        struct sockaddr_in clientAddr;
        socklen_t len = sizeof(clientAddr);
        int fd = accept(serverSocket, (SA *)&clientAddr, &len);
        if (fd == -1)
        {
            printf("客户端连接出错...\n");
            continue;
        }
        int i = 0;
        for (i = 0; i < size; i++)
        {
            if (fds[i] == 0)
            {
                //记录客户端的socket
                fds[i] = fd;
                printf("fd = %d\n", fd);
                //有客户端连接之后，启动线程给此客户服务
                pthread_t tid;
                pthread_create(&tid, 0, service_thread, &fd);
                break;
            }
            if (size == i)
            {
                //发送给客户端说聊天室满了
                char *str = "对不起，聊天室已经满了!";
                send(fd, str, strlen(str), 0);
                close(fd);
            }
        }
    }
}

int main()
{
    init();
    service();
}
