/*socket tcp客户端 */
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "passwdenter.h"

#define SERVER_PORT 5555

typedef struct userdata
{
    char username[10];
    char password[7];
} Userdata;

/*
 连接到服务器后，会不停循环，等待输入，
 输入quit后，断开与服务器的连接
 */

int main()
{
    Userdata *temp = (Userdata *)malloc(sizeof(Userdata));

    char *buffer = (char *)malloc(sizeof(Userdata));
    //客户端只需要一个套接字文件描述符，用于和服务器通信
    int clientSocket;
    //描述服务器的socket
    struct sockaddr_in serverAddr;
    char sendbuf[200];
    char recvbuf[200];
    char username[10];
    char password[7];
    int iDataNum;
    if ((clientSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket");
        return 1;
    }

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(SERVER_PORT);
    //指定服务器端的ip，本地测试：127.0.0.1
    // inet_addr()函数，将点分十进制IP转换成网络字节序IP
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    if (connect(clientSocket, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)
    {
        perror("connect");
        return 1;
    }

    printf("connect with destination host successfully\n");

    char option;
    printf("1. login\n2. register\n");
    printf("Input your option: ");
    while (1)
    {
        scanf("%c", &option);
        if (option == '1')
        {
            printf("this is login\n");
            break;
        }
        else if (option == '2')
        {
            printf("this is register\n");
            break;
        }
        else
        {
            printf("the option you input is invalid\nplease input again: ");
            continue;
        }
    }
    send(clientSocket, &option, sizeof(char), 0);
    printf("请输入你的用户名: ");
    scanf("%s", temp->username);
    passwd(temp->password);
    printf("\n");
    // printf("the username is %s\nthe password is %s\n", temp->username, temp->password);
    // printf("the size of Userdata is %ld: ", sizeof(Userdata));
    memcpy(buffer, temp, sizeof(Userdata));
    // printf("the buffer is: ");
    // for (int j = 0; j < sizeof(Userdata) / sizeof(char); j++)
    //     printf("%c", buffer[j]);

    send(clientSocket, buffer, sizeof(Userdata), 0);
    // while (1)
    // {

    //     iDataNum = recv(clientSocket, buffer, 200, 0);
    //     recvbuf[iDataNum] = '\0';
    //     printf("recv data of my world is: %s\n", recvbuf);
    // }
    close(clientSocket);
    return 0;
}
pzy : dfdafsepzy : vasdfaw2345 : bye2345退出了聊天室