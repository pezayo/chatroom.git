
#include <pthread.h>
#include <unistd.h>

#include <stdio.h>

void *func2(void *p)
{
    pthread_t parentid = *static_cast<pthread_t *>(p);
    printf("in subsubthread, the parent is %d, this thread id is %d\n", parentid, pthread_self());
    pthread_cancel(parentid);
    printf("cancle the parentthread\n");
    while (true)
    {
        sleep(1);
        printf("in the subsub loop\n");
    }
}

void *func(void *arg)
{
    pthread_t main_tid = *static_cast<pthread_t *>(arg);
    pthread_cancel(main_tid);
    pthread_t this_id = pthread_self();
    pthread_t tid;
    printf("in subthread, the parent is %d, this threadid is %d, the sub is %d\n", *static_cast<int *>(arg), this_id, tid);
    pthread_create(&tid, NULL, func2, &this_id);
    while (true)
    {
        printf("in the sub loops\n");
        sleep(0.5);
        // printf("child loops\n");
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_t main_tid = pthread_self();
    pthread_t tid = 0;
    pthread_create(&tid, NULL, func, &main_tid);
    pthread_create(&tid, NULL, func, &main_tid);
    printf("in the main thread, the id is %d\n", main_tid);
    while (true)
    {
        printf("main loops\n");
        sleep(1);
    }
    sleep(1);
    printf("main exit\n");
    return 0;
}